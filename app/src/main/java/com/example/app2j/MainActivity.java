package com.example.app2j;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Button btnCalcular, btnSalir,btnLimpiar;
        TextView lblImc;
        EditText txtAltura, txtPeso;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        txtPeso = findViewById(R.id.txtPeso);
        txtAltura = findViewById(R.id.txtAltura);
        lblImc = findViewById(R.id.lblImc);

        btnCalcular.setOnClickListener(view->{
            if(txtAltura.getText().toString().matches("")) {
                Toast.makeText(MainActivity.this, "Ingresa tu altura", Toast.LENGTH_SHORT).show();
                return;
            }
            if(txtPeso.getText().toString().matches("")){
                Toast.makeText(MainActivity.this, "Ingresar tu peso", Toast.LENGTH_SHORT).show();
                return;
            }
            float altura = Float.parseFloat(txtAltura.getText().toString());
            float peso = Float.parseFloat(txtPeso.getText().toString());
            altura = (float) Math.pow(altura, 2);
            float imc = peso / altura;
            String strImc = "El IMC es: " + imc;

            lblImc.setText(strImc);
        });
        btnLimpiar.setOnClickListener(view -> {
            txtAltura.setText("");
            txtPeso.setText("");
            lblImc.setText("");
            txtAltura.requestFocus();
        });

        btnSalir.setOnClickListener(view->finish());
    }
}